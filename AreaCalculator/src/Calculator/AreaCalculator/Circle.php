<?php
/**
 * Created by PhpStorm.
 * User: Al-amin
 * Date: 22-Mar-18
 * Time: 11:05 AM
 */

namespace Pondit\Calculator\AreaCalculator;


class Circle
{
    public $radius;
    public $pi;

    public function __construct($radius,$pi)
    {
        $this->radius=$radius;
        $this->pi=$pi;
    }

    public function circle1(){
       $radiusValue=$this->radius*$this->radius*$this->pi;
       return $radiusValue;
    }
}