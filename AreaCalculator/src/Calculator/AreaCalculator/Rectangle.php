<?php
/**
 * Created by PhpStorm.
 * User: Al-amin
 * Date: 22-Mar-18
 * Time: 11:04 AM
 */

namespace Pondit\Calculator\AreaCalculator;


class Rectangle
{
    public $width;
    public $height;

 //   public function __construct($width,$height)
//    {
//
//        $this->width=$width ;
//        $this->height=$height;
//    }

    static function getArea($width, $height){
        return $width * $height;
    }
}