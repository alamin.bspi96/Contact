<?php
/**
 * Created by PhpStorm.
 * User: Al-amin
 * Date: 22-Mar-18
 * Time: 11:06 AM
 */

namespace Pondit\Calculator\AreaCalculator;


class Square
{
    public $side;
    public $power;

    public function __construct($side,$power)
    {
        $this->side=$side;
        $this->power=$power;
    }
    public function square1(){
        $square=$this->side**$this->power;
        return $square;
    }
}