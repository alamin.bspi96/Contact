<?php

Class Rectangle{

    public $width;
    public $length;

    Public function __construct($width,$length)
    {
       $this->width=$width;
       $this->length=$length;
    }

    public function getArea(){
        $area=$this->width * $this->length;
        return $area."<br>";

    }

}



$rectangle1=new Rectangle(100,200);
echo $rectangle1->getArea();

/*$rectangle1->width=100;
$rectangle1->length=200;*/

$rectangle2=new Rectangle(35,67);
echo $rectangle2->getArea();

/*$rectangle2=new Rectangle();
$rectangle2->getArea(35,67);*/